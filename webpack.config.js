module.exports = {
    /* ビルドの起点となるファイルの設定 */
    entry: './src/app.js',
    externals: {
        'Config': JSON.stringify(process.env.ENV === 'production' ? {
            serverUrl: "https://unkouno.herokuapp.com",
            wsUrl: "wss://unkouno.herokuapp.com",
        } : {
            serverUrl: "http://localhost:4567",
            wsUrl: "ws://localhost:4567",
        })
    },
    /* 出力されるファイルの設定 */
    output: {
        path: './app/public', // 出力先のパス
        filename: 'bundle.js' // 出力先のファイル名
    },
    /* ソースマップをファイル内に出力させる場合は以下を追加 */
    devtool: 'inline-source-map',
    module: {
        /* loaderの設定 */
        loaders: [
            {
                test: /\.js$/, // 対象となるファイルの拡張子（正規表現可）
                exclude: /node_modules/, // 除外するファイル/ディレクトリ（正規表現可）
                loader: 'babel-loader' // 使用するloader
            }
        ]
    },
    devServer: {
        historyApiFallback: true
    }
};
