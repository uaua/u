require 'test/unit'

require './lib/uno.rb'

class CardTest < Test::Unit::TestCase
  def test_initialize
    color = 114
    number = 514
    c = Card.new(color, number)
    assert_equal c.color, c.color
    assert_equal c.number, c.number
  end
end

class UserTest < Test::Unit::TestCase
  def test_initialize
    user_id = "1"
    u = User.new(user_id, nil)
    assert_equal u.id, user_id
    assert_equal u.hands.length, 0
  end
  
  def test_add_hand
    u = User.new("1", nil)
    c = Card.new(114, 514)

    u.add_hand Card.new(114, 514)

    assert_equal u.hands.length, 1
    assert_equal u.hands[0].color, c.color
    assert_equal u.hands[0].number, c.number
  end

  def test_ok
    u = User.new("1", nil)
    
    assert !u.ok?

    u.ok!
    assert u.ok?
  end
end

class RoomTest < Test::Unit::TestCase
  def test_play_valid
    room = Room.new ""
    room.color = 15
    room.instance_variable_set(:@field, [Card.new(15, 0)])
    room.player = 0

    u1 = User.new("1", nil)
    u1.add_hand Card.new(15, 0)
    u2 = User.new("2", nil)
    u3 = User.new("3", nil)
    room.add_user u1
    room.add_user u2
    room.add_user u3

    assert !room.play([0], {})
  end

  def test_play_invalid
    room = Room.new ""
    room.color = 15
    room.instance_variable_set(:@field, [Card.new(15, 0)])
    room.player = 0

    u1 = User.new("1", nil)
    u1.add_hand Card.new(15, 0)
    u2 = User.new("2", nil)
    u2.add_hand Card.new(15, 0)
    u3 = User.new("3", nil)
    room.add_user u1
    room.add_user u2
    room.add_user u3

    assert room.play([0], {})
  end

  def test_rank
    room = Room.new ""
    room.color = 15
    room.instance_variable_set(:@field, [Card.new(15, 0)])
    room.player = 0

    u1 = User.new("1", nil)
    u1.add_hand Card.new(15, 0)
    u2 = User.new("2", nil)
    u2.add_hand Card.new(15, 0)
    u3 = User.new("3", nil)
    u3.add_hand Card.new(15, 0)
    room.add_user u1
    room.add_user u2
    room.add_user u3

    assert room.play([0], {})
    assert room.play([0], {})
    assert_equal room.instance_variable_get(:@rank), [1, 2, 3]
  end

  def test_all_ok
    room = Room.new ""
    u1 = User.new("1", nil)
    u2 = User.new("2", nil)
    room.add_user u1
    room.add_user u2

    assert !room.all_ok?

    u1.ok!
    assert !room.all_ok?

    u2.ok!
    assert room.all_ok?
  end
end
