import React from 'react';
import ReactDOM from 'react-dom';
import request from 'superagent';
import { IndexRoute, Link, Router, Route, browserHistory } from 'react-router';
import Config from 'Config';

class RoomItem extends React.Component {
    constructor(props) {
        super(props);
    }
    
    render() {
        var msg = this.props.started ? "hajimari" : "mada";
        return (
                <div>
                <Link activeClassName="active" to={`/room/${this.props.name}`}>{this.props.name}: {msg} {this.props.players}人</Link>
                </div>
        );
    }
}

class Card extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {color, number} = this.props;
        var t = {
            1: 0,
            2: 3,
            4: 2,
            8: 1,
            15: number == 13 ? 0 : 4
        };
        var n = {
            10: 11,
            11: 10,
            12: 12,
            13: 13,
            14: 13
        };
        const x = (number > 9 ? n[number] : number) * 42.5 - (number > 9 ? n[number] : number)/4.0;
        const y = t[color] * 64;
        return (<span style={{
            backgroundPosition: `-${x}px -${y}px`,
            backgroundSize: `595px 512px`,
            backgroundImage: "url(/img/cards.png)",
            display: "inline-block",
            verticalAlign: top,
            height: "64px",
            width: "42.5px"
        }}></span>);
        /*
        return (<svg viewBox={attr} height="64" width="43">
                <defs>
                <clipPath clipPathUnits="objectBoundingBox" className={`clip-${x}-${y}`}>
                <rect x={x} y={y} width="85" height="128" />
                </clipPath>
                </defs>
                <image clipPath={`url(clip-${x}-${y})`} href="/img/cards.png" />
          </svg>);
        */
    }
}

class Room extends React.Component {
    constructor(props) {
        super(props);
        console.log("eeee2");
        this.state = {
            room_info: {
                u: {}
            },
            info: {},
            selectedCards: [],
            selectedColor: 1
        };

    }

    componentWillUnmount() {
        this.ws.close(1000, "");
        clearInterval(this.interval);
    }
    
    onMessage(msg) {
        console.log(msg.data);
        const json = JSON.parse(msg.data);
        if (json.err) {
            console.log(json.err);
        } else if (json.q == "join") {
            this.setState({info: json});
            if (json.token) {
                localStorage.setItem("token", json.token);
            }
        } else if(json.q == "info") {
            this.setState({room_info: json});
        }

        if(json.y == json.p){
            var n = new Notification("Your Turn!");
            if (Notification.permission === 'default') {
                // 許可が取れていない場合はNotificationの許可を取る
                Notification.requestPermission();
            }
        }
    }
    
    componentDidMount() {
        console.log("eeee");
        const { roomId } = this.props.params;

        this.ws = new WebSocket(`${Config.wsUrl}/websocket`);

        this.ws.onopen = () => {
            console.log("connection opened");
            this.ws.send(JSON.stringify({
                q: "join",
                n: roomId,
                token: localStorage.getItem("token")
            }));
        };
        this.ws.onclose = () => {
            console.log("connection closed");
        };
        this.ws.onmessage = this.onMessage.bind(this);
    }

    loadRoomInfoFromServer() {
        const { roomId } = this.props.params;
        request
            .post(`${Config.serverUrl}/info`)
            .send({n: roomId, id: this.state.info.id})
            .end((err, res) => {
                console.log(res.body);
                this.setState({room_info: res.body});
        });
    }

    onOkClicked() {
        const { roomId } = this.props.params;
        this.ws.send(JSON.stringify({
            q: "ok",
            n: roomId
        }));
    }

    onCardClicked(i) {
        console.log("card", i);
        var cards = this.state.selectedCards.slice();
        if(cards.length == 0){
            if((this.state.room_info.c & this.state.room_info.u.h[i].c) != 0
            || this.state.room_info.f.n == this.state.room_info.u.h[i].n){
                cards.push(i);
            }
        }else{
            const index = cards.indexOf(i);
            console.log(index, cards);
            if(index != -1) {
                cards.splice(index, 1);
            }else if(this.state.room_info.u.h[cards[0]].n == this.state.room_info.u.h[i].n){
                cards.push(i);
            }
        }
        this.setState({selectedCards: cards});
    }

    onPlayClicked() {
        const { selectedCards , selectedColor} = this.state;
        if (selectedCards.length > 0) {
            this.ws.send(JSON.stringify({
                q: "play",
                i: selectedCards,
                sc: selectedColor // ここwildの色
            }));
            this.setState({selectedCards: []});
        }
    }

    onPassClicked() {
        console.log("pass");
        this.ws.send(JSON.stringify({
            q: "pass"
        }));
        this.setState({selectedCards: []});
    }

    onSelectedColor(e){
        this.setState({selectedColor: e.target.value});
    }

    getColor(i){
        return{
            1: "red",
            2: "blue",
            4: "green",
            8: "yellow",
            15: "wild"
        }[i];
    }

    getCardName(i){
        if(i <= 9){
            return i;
        }
        return{
            10: "reverse",
            11: "skip",
            12: "draw 2",
            13: "",
            14: "draw 4"
        }[i];
    }

    render() {
        //var msg = this.props.started ? "hajimari" : "mada";
        const { roomId } = this.props.params;
        const { room_info, selectedCards } = this.state;
        const buttonMessage = this.state.room_info.u.ok ? "dame" : "ok";

        if (this.state.room_info.s) {
            var sortedHand = (this.state.room_info.u.h).slice().map((c,i) =>{
                c.i = i;
                return c;
            }).sort(function(a, b){
                if(a.n < b.n) return -1;
                if(a.n > b.n) return 1;
                return 0;
            });

            var hands = sortedHand.map((c, i) => {
                var card = <Card number={c.n} color={c.c} />;
                if (selectedCards.indexOf(c.i) != -1) {
                    return (<div
                            key={i}
                            className="card"
                            onClick={() => this.onCardClicked(c.i)}
                            style={{
                                backgroundColor: 'rgb(43, 163, 212)',
                                padding: '3px',
                                height: '64px',
                                width: '43px'
                            }}>
                            {card}
                           </div>);
                } else {
                    return (<div
                            key={i}
                            className="card"
                            onClick={() => this.onCardClicked(c.i)}
                            style={{
                                padding: '3px',
                                height: '64px',
                                width: '43px'
                            }}>
                            {card}
                           </div>);
                }
            });

            var users = this.state.room_info.us.map((u, i) => {
                var msg = u.n;
                if(this.state.room_info.r[i] != 0){
                    msg = this.state.room_info.r[i] + "位";
                }
                return (<span key={i}>{msg} |</span>);
            });
            var turnMsg = room_info.y == room_info.p ? (<span style={{backgroundColor: 'yellow'}} key={"t"}>
                Your Turn</span>) : "Player"+room_info.p+"\'s Turn";
            var headMsg = (this.state.room_info.r[room_info.y] != 0 ? (this.state.room_info.r[room_info.y] + "位") : ("You're player" + this.state.room_info.y))
            return (
                    <div>
                    <b> <font size="5"> {headMsg} </font> </b>
                    <div>
                    <b>hands</b>
                    <div className="cards">{hands}</div>
                    </div>
                    <div>
                    <button onClick={this.onPlayClicked.bind(this)}>play</button>
                    <button onClick={this.onPassClicked.bind(this)}>pass</button>
                    </div>
                    <div>
                    <b>field</b>
                    <div><Card color={this.state.room_info.f.c} number={this.state.room_info.f.n} /></div>
                    </div>
                    <div>
                    <b>color</b>
                    <div>{this.getColor(this.state.room_info.c)}</div>
                    </div>
                    <div>
                    <b>turn</b>
                    <div><font size="3">{turnMsg}</font></div>
                    </div>
                    <div>
                    <b>player</b>
                    <div>{users}</div>
                    </div>
                    <div>
                        <select onChange={this.onSelectedColor.bind(this)}>
                            <option value="1"> red </option>
                            <option value="2"> blue </option>
                            <option value="4"> green </option>
                            <option value="8"> yellow </option>
                        </select>
                    </div>
                    </div>
            );
        } else {
            return (
                    <div>
                    <h1>{roomId}</h1>
                    <div>
                    <p>人数: {this.state.room_info.num}</p>
                    </div>
                    <div>
                    <button onClick={this.onOkClicked.bind(this)}>{buttonMessage}</button>
                    </div>
                    <Link to="/">modoru!</Link>
                    </div>
            );
        }
    }
}

class Rooms extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          rooms: [],
          name: ''
        };
    }

    loadRoomsFromServer() {
        request.get(`${Config.serverUrl}/rooms`).end((err, res) => {
            this.setState({rooms: res.body});
        });
    }

    onCreateClicked() {
        console.log("onCreateClicled()");
        request
            .post(`${Config.serverUrl}/create`)
            .send({n: this.state.name})
            .end((err, res) => {
                this.loadRoomsFromServer();
            });
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
    
    componentDidMount() {
        this.loadRoomsFromServer();
        this.interval = setInterval(this.loadRoomsFromServer.bind(this), 1000);
    }

    handleNameChange(event) {
        this.setState({name: event.target.value});
    }
  
    render() {
        var rooms = this.state.rooms.map((room, i) => {
            return <RoomItem key={i} name={room.n} started={room.s} players={room.p} />;
        });
        return (
                <div>
                <input value={this.state.name} onChange={this.handleNameChange.bind(this)} type="text" id="room-name" />
                <button onClick={this.onCreateClicked.bind(this)}>Create</button>
                <div>
                {rooms}
            </div>
                {this.props.children}
            </div>);
    }
}

ReactDOM.render((
        <Router history={browserHistory}>
        <Route path="/" component={Rooms}>
        </Route>
        <Route path="/room/:roomId" component={Room}>
        </Route>
        </Router>
), document.querySelector(".content"));
