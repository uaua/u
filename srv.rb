require 'thread'
require 'sinatra'
require 'sinatra/cross_origin'
require 'sinatra-websocket'
require 'json'

require './lib/uno.rb'

register Sinatra::CrossOrigin

configure do
  enable :cross_origin
end

rooms = {}

before do
  if request.request_method == 'OPTIONS'
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Methods"] = "POST"

    halt 200
  end
end

set :root, './app'

post '/create' do
  content_type :json
  name = JSON.parse(request.body.read)["n"]
  return {err: "name kabuteru"}.to_json if rooms.key?(name) || name.chomp.length == 0
  rooms[name] = Room.new(name)
  data = { n: rooms.length }
  data.to_json
end

get '/websocket' do
  if request.websocket? then
    request.websocket do |ws|
      id = ""
      room_id = -1
      user = {}
      ws.onopen do
        id = ((0..9).to_a + ("a".."z").to_a + ("A".."Z").to_a).sample(10).join
        user = User.new(id, ws)
        puts "id: #{id}"
      end

      ws.onmessage do |msg|
        json = JSON.load(msg)
        case json["q"]
        when "join"
          room_id = json["n"].chomp
          unless (room = rooms[room_id])
            room = rooms[room_id] = Room.new(room_id)
            # ws.send({err: "naiyo"}.to_json)
            # next
          end
          if room.started
            nu = room.users.find {|u| u.token == json["token"] }

            if nu && !nu.ok?
              user = nu
              user.ws = ws
              id = user.id
              user.ok!
              puts "user: #{user.ok} #{user.token}"
              ws.send({
                        q: json["q"],
                        n: room_id,
                        id: id
                      }.to_json)
              room.users.each.with_index { |u, i|
                a = room.to_a(i);
                a["q"] = "info";
                u.ws.send(a.to_json)
              }
              next
            else
              ws.send({err: "mou hajimateru"}.to_json)
              next
            end
          end
          room.add_user user
          ws.send({
                    q: json["q"],
                    n: room_id,
                    id: id,
                    token: user.token
                  }.to_json)
          room.users.each.with_index { |u, i|
            a = room.to_a(i);
            a["q"] = "info";
            u.ws.send(a.to_json)
          }

        when "ok"
          unless (room = rooms[room_id])
            ws.send({err: "naiyo"}.to_json)
            next
          end
          if room.started
            ws.send({err: "mou hajimateru"}.to_json)
            next
          end
          user.ok!
          i = room.users.index {|u| u.id == id }
          if !room.started && room.all_ok?
            room.start
            room.users.each.with_index { |u, i|
              a = room.to_a(i);
              a["q"] = "info";
              u.ws.send(a.to_json)
            }
          else
            a = room.to_a(i);
            a["q"] = "info";
            ws.send(a.to_json)
          end

        when "play"
          unless (room = rooms[room_id])
            ws.send({err: "naiyo"}.to_json)
            next
          end
          i = room.users.index {|u| u.id == id }
          unless room.started
            ws.send({err: "mada hajimatenai"}.to_json)
            next
          end

          unless i == room.player
            ws.send({err: "omae ja nai"}.to_json)
            next
          end

          hi = json["i"].map(&:to_i)
          unless room.is_playable_cards(hi)
            ws.send({err: "dasenu"}.to_json)
            next
          end

          if room.play(hi, json)
            room.users.each.with_index { |u, i|
              a = room.to_a(i);
              a["q"] = "info";
              u.ws.send(a.to_json)
            }
          end

        when "pass"
          unless (room = rooms[room_id])
            ws.send({err: "naiyo"}.to_json)
            next
          end
          i = room.users.index {|u| u.id == id }
          unless room.started
            ws.send({err: "mada hajimatenai"}.to_json)
            next
          end

          unless i == room.player
            ws.send({err: "omae ja nai"}.to_json)
            next
          end

          room.pass
          room.users.each.with_index { |u, i|
            a = room.to_a(i);
            a["q"] = "info";
            u.ws.send(a.to_json)
          }

        end
      end

      ws.onclose do
        puts "close"
        unless (room = rooms[room_id])
          ws.send({err: "naiyo"}.to_json)
          next
        end
        if room.started?
          lu = room.users.find { |u|
            u.id == id
          }
          if lu
            lu.ok!
          end
        else
          room.users.delete_if { |u|
            u.ws == ws
          }
          if room.users.empty?
            rooms.delete room_id
            next
          end
        end
        room.users.each.with_index { |u, i|
          a = room.to_a(i);
          a["q"] = "info";
          u.ws.send(a.to_json)
        }
      end
    end
  end
end

get '/rooms' do
  content_type :json
  data = []
  rooms.each { |n, room|
    data << {
      s: room.started,
      p: room.users.length,
      n: n
    }
  }
  data.to_json
end

get '/*' do
  render :html, :index
end
