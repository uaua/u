class Card
  attr_reader :color, :number
  def initialize color, number
    @color = color
    @number = number
  end

  def self.gen_cards
    cards = []
    4.times { |c|
      cards << Card.new(1<<c, 0)
    }
    4.times { |c|
      2.times {
        1.upto(12) { |n|
          cards << Card.new(1<<c, n)
        }
      }
    }
    13.upto(14) { |n|
      2.times {
        cards << Card.new(15, n)
      }
    }
    cards
  end

  def to_s
    c = {1 => "r", 2 => "b", 4 => "y", 8 => "g", 15 => "w"}[@color]
    n = if @number > 9
          ["r", "s", "d", "w", "wd"][@number-10]
        else
          @number
        end
    "#{c} #{n}"
  end
end

class User
  attr_accessor :id, :hands, :ws, :ok, :token
  def initialize id, ws
    @id = id
    @token = ((0..9).to_a + ("a".."z").to_a + ("A".."Z").to_a).sample(32).join
    @hands = []
    @ws = ws
    @ok = false
  end

  def add_hand c
    hands << c
  end

  def ok!
    @ok = !@ok
  end

  def ok?
    @ok
  end
  
  def to_a
    {
      i: @id,
      h: @hands.map {|c| {c: c.color, n: c.number} },
      ok: @ok
    }
  end
end

class Room
  attr_accessor :name, :users, :player, :started, :color
  def initialize name
    @name = name
    @users = []
    @deck = Card.gen_cards.shuffle
    @field = []
    @rank = []
    @color = 0
    @started = false
    @draw = 0
    @draw_type = 0
    @next = 0
    @pass_state = 0
    @ord = 0
  end

  def all_ok?
    @users.all? { |u| u.ok }
  end

  def started?
    @started
  end
  
  def to_a i
    {
      u: @users[i].to_a,
      us: @users.map do |u|
        {n: u.hands.size}
      end,
      r: @rank,
      f: @field.empty? ? nil : {c: @field.last.color, n: @field.last.number},
      c: @color,
      num: @users.length,
      d: @draw,
      dt: @draw_type,
      s: @started,
      p: @player,
      ps: @pass_state,
      y: i,
      o: @ord,
      done: game_is_done?
    }
  end
  
  def add_user user
    @users << user
    @rank << 0
  end
  
  def start
    @started = true
    @player = 0 #rand(@users.length)
    @users.each { |u|
      7.times {
        u.add_hand @deck.shift
      }
    }
    @deck.shuffle while @deck.first.number >= 13
    @field << @deck.shift
    @color = @field.first.color
  end

  def to_s
    res = "----------------------------\n"

    res << "draw stack: #{@draw}\n"
    res << "ord: #{@ord}\n"
    res << "color: #{@color}\n"
    res << "player: #{@player}\n"
    res << "rank: #{@rank.join(" ")}\n"

    res << "users:\n"
    @users.each.with_index { |u, i|
      res << "#{i} |"
      u.hands.each { |n|
        res << "#{n.to_s}|"
      }
      res << "\n"
    }

    res << "\ndeck:\n"
    @deck.each { |n|
      res << "#{n.to_s}|"
    }
    res << "\n"

    res << "\nfield:\n"
    @field.each { |n|
      res << "#{n.to_s}|"
    }
    res << "\n"

    return res
  end

  def is_playable_card c
    (@draw == 0 && ((@color&c.color) != 0 || @field.last.number == c.number)) ||
      (@draw != 0 && (c.number == 12 || c.number == 14))
  end

  def is_playable_cards ca
    return false unless is_playable_card(@users[@player].hands[ca.first])

    if ca.any? do |i|
         @users[@player].hands[ca.first].number != @users[@player].hands[i].number
       end
      return false
    end
    true
  end
  
  def is_playable
    @users[@player].each do |c|
      return true if is_playable_card(c)
    end
    return false
  end

  def draw
    d = @draw > 0
    @draw = 1 unless d
    while @draw > 0
      if @deck.empty?
        l = @field.last
        @deck = @field[0..@field.length-1]
        @deck.shuffle!
      end
      @users[@player].add_hand @deck.shift
      @draw -= 1
    end
  end
  
  def next_player
    @pass_state = (@draw == 0 ? 0 : 2)
    return if available_players_num <= 1
    add = 0
    if @next > 0
      add = (@next*2)
    else
      add = 1
    end
    while add > 0
      if @ord == 0
        @player = (@player+1)%@users.length
      else
        @player = (@player+(users.length-1))%@users.length
      end
      add -= 1 if @users[@player].hands.length > 0
    end
    @next = 0
  end

  def available_players_num
    @users.count do |c| c.hands.length > 0 end
  end
  
  def play_card c, op
    @color = c.color
    case c.number
    when 10
      if available_players_num > 2
        @ord = 1-@ord
      else
        @next += 1
      end
    when 11
      @next += 1
    when 12
      @draw += 2
    when 13
      raise "iro nasi" unless op['sc']
      @color = op['sc'].to_i
    when 14
      raise "iro nasi" unless op['sc']
      @draw += 4
      @color = op['sc'].to_i
    end
  end

  def game_is_done?
    @users.length > 1 && @users.count do |u| u.hands.size != 0 end == 1
  end

  def play is, op
    if is_playable_cards(is) &&
       (!op.key?('sc') || [1, 2, 4, 8].include?(op['sc'].to_i)) &&
       !game_is_done?
      is.each do |i|
        play_card(@users[@player].hands[i], op)
        @field << @users[@player].hands[i]
      end
      iss = is.sort.reverse
      iss.each do |i|
        @users[@player].hands.delete_at(i)
      end

      if @users[@player].hands.size == 0
        @rank[@player] = @users.count do |u| u.hands.size == 0 end
        if @rank[@player] == @users.length-1
          @rank[@users.index do |u| u.hands.size != 0 end] = @users.length
        end
      end

      next_player
      return true
    end
    return false
  end

  def pass
    return if game_is_done?
    if @pass_state == 0
      draw
      @pass_state = 1
    elsif @pass_state == 1
      next_player
    elsif @pass_state == 2
      draw
      next_player
    end
  end
end
